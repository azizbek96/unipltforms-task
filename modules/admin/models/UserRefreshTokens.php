<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "user_refresh_tokens".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $token
 * @property string|null $ip
 * @property string|null $user_agent
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $created_by
 * @property int|null $updated_at
 * @property int|null $updated_by
 */
class UserRefreshTokens extends BaseModel
{
	/**
	 * {@inheritdoc}
	 */
	public static function tableName()
	{
		return 'user_refresh_tokens';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['user_id', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'default', 'value' => null],
			[['user_id', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
			[['token', 'user_agent'], 'string', 'max' => 1000],
			[['ip'], 'string', 'max' => 50],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'user_id' => Yii::t('app', 'User ID'),
			'token' => Yii::t('app', 'Token'),
			'ip' => Yii::t('app', 'Ip'),
			'user_agent' => Yii::t('app', 'User Agent'),
			'status' => Yii::t('app', 'Status'),
			'created_at' => Yii::t('app', 'Created At'),
			'created_by' => Yii::t('app', 'Created By'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'updated_by' => Yii::t('app', 'Updated By'),
		];
	}
}
