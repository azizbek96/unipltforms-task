<?php

namespace app\modules\api\controllers;

use Yii;
use app\models\LoginForm;
use app\controllers\_BaseController;
use app\modules\admin\models\Users;
use yii\web\ServerErrorHttpException;
use app\modules\admin\models\UserRefreshTokens;

/**
 * Default controller for the `api` module
 */
class AuthController extends _BaseController
{

	/**
	 * @return array
	 * @throws ServerErrorHttpException
	 */
	public function actionLogin()
	{
		$model = new LoginForm();
		$data = Yii::$app->request->post();
		if ($model->load($data, '') && $model->login()) {
            /** @var Users $user */
            $user = Yii::$app->user->identity;
			if (!empty($user)) {
				$token = $this->generateJwt($user);
				$this->generateRefreshToken($user);
				return $this->success([
					'user' => [
						'username' => $user->username,
//						'roles' => Yii::$app->authManager->getPermissionsByUser($user->id),
					],
					'token' => (string) $token,
				]);
			} else {
				throw new ServerErrorHttpException('User not found');
			}
		} else {
			return $this->error($model->getFirstErrors());
		}
	}

	public function actionRefreshToken()
	{
		$refreshToken = Yii::$app->request->cookies->getValue('refresh-token', false);

		if (!$refreshToken) {
			return new \yii\web\UnauthorizedHttpException('No refresh token found.');
		}

		$userRefreshToken = UserRefreshTokens::findOne([
			'token' => $refreshToken,
		]);

		if (Yii::$app->request->isPost) {
			// Getting new JWT after it has expired
			if (!$userRefreshToken) {
				return new \yii\web\UnauthorizedHttpException('The refresh token no longer exists.');
			}
			$user = Users::find() //adapt this to your needs
				->where(['id' => $userRefreshToken->user_id])
				->andWhere(['not', ['status' => Users::STATUS_INACTIVE]])
				->one();
			if (!$user) {
				$userRefreshToken->delete();
				return new \yii\web\UnauthorizedHttpException('The user is inactive.');
			}

			$token = $this->generateJwt($user);

			return [
				'status' => 'ok',
				'token' => (string) $token,
			];
		} elseif (Yii::$app->request->isDelete) {
			// Logging out
			if ($userRefreshToken && !$userRefreshToken->delete()) {
				return new ServerErrorHttpException('Failed to delete the refresh token.');
			}

			return ['status' => 'ok'];
		} else {
			return new \yii\web\UnauthorizedHttpException('The user is inactive.');
		}
	}

	private function generateJwt(Users $user)
	{
		$jwt = Yii::$app->jwt;
		$signer = $jwt->getSigner('HS256');
		$key = $jwt->getKey();
		$time = time();

		$jwtParams = Yii::$app->params['jwt'];

		return $jwt
			->getBuilder()
			->issuedBy($jwtParams['issuer'])
			->permittedFor($jwtParams['audience'])
			->identifiedBy($jwtParams['id'], true)
			->issuedAt($time)
			->expiresAt($time + $jwtParams['expire'])
			->withClaim('uid', $user->id)
			->getToken($signer, $key);
	}

	private function generateRefreshToken(Users $user, Users $impersonator = null): UserRefreshTokens
	{
		$refreshToken = Yii::$app->security->generateRandomString(200);

		// TODO: Don't always regenerate - you could reuse existing one if user already has one with same IP and user agent
		$userRefreshToken = new UserRefreshTokens([
			'user_id' => $user->id,
			'token' => $refreshToken,
			'ip' => Yii::$app->request->userIP,
			'user_agent' => Yii::$app->request->userAgent,
		]);
		if (!$userRefreshToken->save()) {
			throw new ServerErrorHttpException('Failed to save the refresh token: ' . $userRefreshToken->getErrorSummary(true));
		}

		// Send the refresh-token to the user in a HttpOnly cookie that Javascript can never read and that's limited by path
		Yii::$app->response->cookies->add(
			new \yii\web\Cookie([
				'name' => 'refresh-token',
				'value' => $refreshToken,
				'httpOnly' => true,
				'sameSite' => 'none',
				'secure' => true,
				'path' => '/v1/auth/refresh-token', //endpoint URI for renewing the JWT token using this refresh-token, or deleting refresh-token
			])
		);

		return $userRefreshToken;
	}
}
