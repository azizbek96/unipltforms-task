<?php

namespace app\modules\api\controllers;

use app\components\PermissionHelper as P;
use Yii;
use app\controllers\BaseController;
use app\modules\admin\models\Users;
use app\modules\api\models\BaseModel;
use app\modules\admin\models\UsersSearch;
use app\modules\hrm\repository\EmployeeRepository;
use app\modules\admin\repository\UsersRepository;
use app\modules\admin\models\AuthAssignmentSearch;
use app\modules\admin\repository\AuthItemRepository;

class UsersController extends BaseController
{
    protected EmployeeRepository $employeeRepository;
    protected AuthItemRepository $authItemRepository;

    public function __construct($id, $module, $config = [],
                                AuthItemRepository $authItemRepository,
                                EmployeeRepository $employeeRepository)
    {
        parent::__construct($id, $module, $config);
        $this->employeeRepository = $employeeRepository;
        $this->authItemRepository= $authItemRepository;
    }

    /**
	 * @param int $page
	 * @return array
	 */
	public function actionIndex(int $page = 1)
	{
		if (Yii::$app->request->isGet) {
            $params = Yii::$app->request->queryParams;
			return $this->getUsers($params, $page);
		} else {
			return $this->error([], Yii::t('app', 'The query is incorrect'));
		}
	}

	/**
	 * @param $id
	 * @return array
	 */
	public function actionView($id)
	{
		if (Yii::$app->request->isGet) {
			return $this->success($this->findModel($id));
		}
		return $this->error([], Yii::t('app', 'The query is incorrect'));
	}

    /**
     * @return array
     */
	public function actionCreate()
	{
		$app = $this->request;
		if ($app->isPost) {
			$model = (new UsersRepository())->saveModel($app->post());
			if ($model['status']) {
				return $this->success((new UsersRepository())->getUser($model['id']));
			} else {
				return $this->error([
					'errors' => $model['errors'],
				]);
			}
		}
		if ($app->isGet) {
			$roleTypes = $this->authItemRepository->getRoleType();
			$roles = $this->authItemRepository->getRolesForSelect();
            $employees = $this->employeeRepository->getList();
			return $this->success([
				'roles' => $roles,
				'roleTypes' => $roleTypes,
				'statusList' => BaseModel::getStatusList(null, false),
				'employeeList' => $employees,
			]);
		}
		return $this->error([], Yii::t('app', 'The query is incorrect'));
	}

	/**
	 * @param $id
	 * @return array
	 */
	public function actionUpdate($id): array
    {
		$app = Yii::$app->request;
		if ($app->isPut) {
			$model = UsersRepository::saveModel($app->post(), $id);
			if ($model['status']) {
				return $this->getUsers();
			} else {
				return $this->error($model);
			}
		}
		if ($app->isGet) {
            $roleTypes = $this->authItemRepository->getRoleType();
			$userRoles = AuthAssignmentSearch::getUserRolesForSelect($id);
            $roles = $this->authItemRepository->getRolesForSelect($userRoles);

			$values = UsersRepository::getUsernameAndHrEmployeeID($id);

            $employees = $this->employeeRepository->getList();
			return $this->success([
				'roles' => $roles,
				'values' => $values,
				'userRoles' => $userRoles,
                'statusList' => BaseModel::getStatusList(null, false),
				'roleTypes' => $roleTypes,
                'employeeList' => $employees,
			]);
		}

		return $this->error([], Yii::t('app', 'The query is incorrect'));
	}

	/**
	 * @param $id
	 * @return array
	 */
	public function actionDelete($id): array
	{
		if (Yii::$app->request->isDelete) {
			$model = $this->findModel($id);
			$model->status = BaseModel::STATUS_DELETED;
			if ($model->save()) {
				return $this->getUsers();
			} else {
				return $this->error($model->getErrors());
			}
		} else {
			return $this->error([], Yii::t('app', 'The query is incorrect'));
		}
	}

	/**
	 * @param $id
	 * @return Users|array|null
	 */
	protected function findModel($id)
	{
		$model = Users::find()
			->where(['id' => $id])
			->andWhere(['!=', 'status', BaseModel::STATUS_DELETED])
			->one();
		if ($model !== null) {
			return $model;
		} else {
			return $this->error();
		}
	}

	/**
	 * @param int $page
	 * @return array
	 */
	private function getUsers(array $params=[], int $page = 1): array
	{
		$searchModel = new UsersSearch();
		$dataProvider = $searchModel->search($params, $page);
		$totalCount = $dataProvider->totalCount;
		$pagination = $dataProvider->pagination;
		return $this->success($dataProvider, [
			'statusList' => BaseModel::getStatusList(),
			'pagination' => [
				'totalSize' => $totalCount,
				'page' => $pagination->page + 1,
				'sizePerPage' => $pagination->pageSize,
				'pageCount' => ceil($totalCount / $pagination->pageSize),
			],
            'permissions' => [
                'create' => P::can('users/create'),
                'view' => P::can('users/view'),
                'delete' => P::can('users/delete'),
                'update' => P::can('users/update'),
            ],
		]);
	}
}
