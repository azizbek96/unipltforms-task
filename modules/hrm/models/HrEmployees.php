<?php

namespace app\modules\hrm\models;

use app\modules\admin\models\Users;
use app\modules\api\models\BaseModel;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "hr_employees".
 *
 * @property int $id
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $father_name
 * @property string|null $address
 * @property string|null $phone_number
 * @property string|null $passport
 * @property string|null $position
 * @property int|null $type
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $created_by
 * @property int|null $updated_at
 * @property int|null $updated_by
 * @property int|null $company_id
 *
 * @property Company $company
 * @property Users[] $users
 */
class HrEmployees extends \app\modules\hrm\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hr_employees';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address'], 'string'],
            [['type', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by', 'company_id'], 'integer'],
            [['first_name', 'last_name', 'father_name'], 'string', 'max' => 255],
            [['phone_number', 'passport', 'position'], 'string', 'max' => 50],
            ['passport', 'match', 'pattern' => '/^[A-Z]{2}[0-9]{7}$/'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'father_name' => Yii::t('app', 'Father Name'),
            'address' => Yii::t('app', 'Address'),
            'phone_number' => Yii::t('app', 'Phone Number'),
            'passport' => Yii::t('app', 'Passport'),
            'position' => Yii::t('app', 'Position'),
            'type' => Yii::t('app', 'Type'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'company_id' => Yii::t('app', 'Company ID'),
        ];
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @param $insert
     * @return bool
     */
    public function beforeSave($insert): bool
    {
        if ($this->isNewRecord) {
            $this->status = BaseModel::STATUS_ACTIVE;
        }

        return parent::beforeSave($insert);
    }



    /**
     * Gets query for [[Users]].
     *
     * @return ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::class, ['hr_employee_id' => 'id']);
    }

    /**
     * @return array
     */
    public function getCreate(): array
    {
        return [
            'id' => $this->id,
            'address' => $this->address,
            'last_name' => $this->last_name,
            'first_name' => $this->first_name,
            'father_name' => $this->father_name,
            'company_id' => $this->company_id,
            'company_name' => $this->company->name ?? '',
        ];
    }

    /**
     * @param $id
     * @return array
     */
    public function getUpdate($id)
    {
        $model = $this->getAttributes();
        $model['company_name'] = $this->company->name??'';
        unset($model['created_at'], $model['created_by'], $model['updated_at'], $model['updated_by'], $model['type']);
        return [
            'model' => $model,
        ];
    }

    /**
     * @return array|bool[]
     */
    public function isDelete(): array
    {
        $this->status = BaseModel::STATUS_DELETED;
        if ($this->save(false)) {
            return [
                'status' => true,
            ];
        } else {
            return [
                'status' => false,
                'error' => $this->getErrors(),
            ];
        }
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return ucfirst($this->last_name) . ' ' . ucfirst($this->first_name) . ' ' . ucfirst($this->father_name);
    }
}