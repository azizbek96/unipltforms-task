<?php

use yii\db\Migration;

/**
 * Class m220719_045055_insert_rbac_permissions
 */
class m220719_045055_insert_rbac_permissions extends Migration
{
    static $module = 'api';
    static $controller = 'users';
    static $actions = ['index', 'create', 'update', 'delete', 'view'];
    static $roleName = 'role-user';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->upsert('auth_item', ['name' => self::$roleName, 'type' => '1', 'description' => '','name_for_user'=>'User uchun ruxsatlar']);
        foreach (self::$actions as $action) {
            $p = self::$controller . '/' . $action;
            $this->upsert('auth_item', ['name' => $p, 'type' => '2', 'description' => '']);
            $this->upsert('auth_item_child', ['parent' => self::$roleName, 'child' => $p]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        foreach (self::$actions as $action) {
            $p =  self::$controller . '/' . $action;
            $this->delete('auth_item', ['name' => $p, 'type' => '2']);
            $this->delete('auth_item_child', ['parent' => self::$roleName, 'child' => $p]);
        }
        $this->delete('auth_item', ['name' => self::$roleName, 'type' => '1']);
    }

}
