<?php

namespace app\modules\api\controllers;

use app\components\PermissionHelper as P;
use app\modules\hrm\models\Company;
use Yii;
use yii\db\Exception;
use app\controllers\BaseController;
use app\modules\api\models\BaseModel;
use app\modules\hrm\models\HrEmployees;
use app\modules\hrm\models\search\HrEmployeesSearch;
use app\modules\hrm\repository\EmployeeRepository;

class EmployeeController extends BaseController
{
    protected EmployeeRepository $employeeRepository;
    public function __construct($id, $module, $config = [], EmployeeRepository $employeeRepository )
    {
        $this->employeeRepository = $employeeRepository;
        parent::__construct($id, $module, $config);
    }

    /**
	 * @param int $page
	 * @return array
	 * @throws Exception
	 */
	public function actionIndex(int $page = 1): array
	{
		if ($this->request->isGet) {
			return $this->getEmployee($page);
		}
		return $this->error([], Yii::t('app', 'The query is incorrect'));
	}

	/**
	 * @param $id
	 * @return HrEmployees|array|null
	 */
	public function actionView($id)
	{
		if ($this->request->isGet) {
			$model = $this->findModel($id);
			if (!empty($model['code']) && $model['code'] == BaseModel::CODE_ERROR) {
				return $model;
			} else {
				return $this->success($model);
			}
		}
		return $this->error();
	}

	/**
	 * @return array
	 */
	public function actionCreate()
	{
		if ($this->request->isPost) {
			return $this->extracted(null);
		}
        if ($this->request->isGet){

            return $this->success(['company' => Company::currentCompany()]);
        }
		return $this->error();
	}

    /**
     * @param $id
     * @return array
     */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if (empty($model)) {
			return $this->error([], Yii::t('app', 'Not Found'), 404);
		}

		if ($this->request->isPut) {
			return $this->extracted($id);
		}

		if ($this->request->isGet) {
			return $this->success($model->getUpdate($id));
		}
		return $this->error();
	}

	/**
	 * @param $id
	 * @return HrEmployees|array|null
	 */
	public function actionDelete($id)
	{
		if ($this->request->isDelete) {
			$model = $this->findModel($id);
			if (!empty($model['code']) && $model['code'] == BaseModel::CODE_ERROR) {
				return $model;
			} else {
				$response = $model->isDelete();
				return $response['status'] ? $this->getEmployeePage($page = 1, "Employee successfuly deleted") : $this->error($response['errors']);
			}
		} else {
			return $this->error();
		}
	}
	/**
	 * @param $id
	 * @return array
	 */
	private function extracted($id = null): array
	{
		$data = $this->request->post();
		$response = $this->employeeRepository->saveModel($data, $id);
		return $response['status'] ? $this->success($response['model'], [], Yii::t('app', 'Employee successfuly created')) : $this->error($response);
	}
	/**
	 * @param $id
	 * @return HrEmployees|array|null
	 */
	protected function findModel($id)
	{
		$model = $this->employeeRepository->findModel($id);
		return $model !== null ? $model : $this->error();
	}

	/**
	 * @param int $page
	 * @param null $departmentId
	 * @return array
	 */
	private function getEmployee(int $page = 1): array
	{
		$searchModel = new HrEmployeesSearch();
		$dataProvider = $searchModel->search($this->request->queryParams, $page);
		return $this->success($dataProvider['dataProvider'], [
			'pagination' => $dataProvider['pagination'],
            'permissions' => [
                'create' => P::can('employee/create'),
                'view' => P::can('employee/view'),
                'delete' => P::can('employee/delete'),
                'update' => P::can('employee/update'),
            ],
		]);
	}


	/**
	 * @param int $page
	 * @param null $departmentId
	 * @return array
	 */
	private function getEmployeePage(int $page = 1, $message = ''): array
	{
		$searchModel = new HrEmployeesSearch();
		$dataProvider = $searchModel->page($page);

		return $this->success($dataProvider['dataProvider'], [
			'pagination' => $dataProvider['pagination'],
		], $message);
	}
}
