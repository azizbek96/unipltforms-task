<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%hr_employees}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%company}}`
 */
class m220930_011259_add_company_id_column_to_hr_employees_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%hr_employees}}', 'company_id', $this->integer());

        // creates index for column `company_id`
        $this->createIndex(
            '{{%idx-hr_employees-company_id}}',
            '{{%hr_employees}}',
            'company_id'
        );

        // add foreign key for table `{{%company}}`
        $this->addForeignKey(
            '{{%fk-hr_employees-company_id}}',
            '{{%hr_employees}}',
            'company_id',
            '{{%company}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%company}}`
        $this->dropForeignKey(
            '{{%fk-hr_employees-company_id}}',
            '{{%hr_employees}}'
        );

        // drops index for column `company_id`
        $this->dropIndex(
            '{{%idx-hr_employees-company_id}}',
            '{{%hr_employees}}'
        );

        $this->dropColumn('{{%hr_employees}}', 'company_id');
    }
}
