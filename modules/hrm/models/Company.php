<?php

namespace app\modules\hrm\models;

use app\modules\api\models\BaseModel;
use Yii;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $leader_name
 * @property string|null $address
 * @property string|null $email
 * @property string|null $website
 * @property string|null $phone_number
 * @property int|null $type
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $created_by
 * @property int|null $updated_at
 * @property int|null $updated_by
 *
 * @property HrEmployees[] $hrEmployees
 */
class Company extends \app\modules\hrm\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address'], 'string'],
            [['type', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['name', 'leader_name', 'email', 'website', 'phone_number'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'leader_name' => Yii::t('app', 'Leader Name'),
            'address' => Yii::t('app', 'Address'),
            'email' => Yii::t('app', 'Email'),
            'website' => Yii::t('app', 'Website'),
            'phone_number' => Yii::t('app', 'Phone Number'),
            'type' => Yii::t('app', 'Type'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * Gets query for [[HrEmployees]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHrEmployees()
    {
        return $this->hasMany(HrEmployees::className(), ['company_id' => 'id']);
    }
    /**
     * @return array
     */
    public function getCreate(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'leader_name' => $this->leader_name,
            'address' => $this->address,
            'email' => $this->email,
            'website' => $this->website,
            'phone_number' => $this->phone_number,
        ];
    }
    /**
     * @param $id
     * @return array
     */
    public function getUpdate($id)
    {
        $model = $this->getAttributes();
        unset($model['created_at'], $model['created_by'], $model['updated_at'], $model['updated_by'], $model['type']);
        return [
            'model' => $model,
        ];
    }
    /**
     * @return array|bool[]
     */
    public function isDelete(): array
    {
        $this->status = BaseModel::STATUS_DELETED;
        if ($this->save(false)) {
            return [
                'status' => true,
            ];
        } else {
            return [
                'status' => false,
                'error' => $this->getErrors(),
            ];
        }
    }

    public static function currentCompany()
    {
        $company_id = \Yii::$app->user->identity->hrEmployee->company_id;
        return Company::find()->where(['status' => BaseModel::STATUS_ACTIVE])->filterWhere(['id' => $company_id])->asArray()->all();
    }
}

