<?php

namespace app\components;

use Yii;
use yii\base\ExitException;
use yii\filters\Cors;

class CorsCustom extends Cors
{
	/**
	 * @throws ExitException
	 */
	public function beforeAction($action)
	{
		parent::beforeAction($action);

		if (Yii::$app->request->isOptions) {
			Yii::$app->response->headers->set(
				'Access-Control-Expose-Headers',
				'X-Pagination-Current-Page, X-Pagination-Per-Page, X-Pagination-Total-Count,X-Pagination-Page-Count X-Pagination-Page-Range'
			);

			Yii::$app->end();
		}
		return true;
	}
}
