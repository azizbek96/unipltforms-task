<?php

namespace app\modules\hrm\models\search;

use app\modules\hrm\models\BaseModel;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\hrm\models\Company;
use yii\data\SqlDataProvider;

/**
 * CompanySearch represents the model behind the search form of `app\modules\hrm\models\Company`.
 */
class CompanySearch extends Company
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'leader_name', 'address', 'email', 'website', 'phone_number'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, int $page = 1)
    {
        $query = Company::find();


        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'leader_name', $this->leader_name])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'website', $this->website])
            ->andFilterWhere(['like', 'phone_number', $this->phone_number]);

        $command = $query->createCommand();
        $dataProvider = new SqlDataProvider([
            'sql' => $command->rawSql,
            'pagination' => [
                'pageSize' => 55,
                'page' => $page - 1,
            ],
            'sort' => [
                'attributes' => ['id', 'address', 'leader_name', 'first_name', 'father_name'],
            ],
        ]);
        $totalCount = $dataProvider->getTotalCount();
        $pagination = $dataProvider->getPagination();
        return [
            'dataProvider' => $dataProvider->getModels(),
            'pagination' => [
                'totalSize' => $totalCount,
                'page' => $pagination->page + 1,
                'sizePerPage' => $pagination->pageSize,
                'pageCount' => ceil($totalCount / $pagination->pageSize),
            ],
        ];
    }

    /**
     * @param $params
     * @param int $page
     * @param null $departmentId
     * @return array
     */
    public function page(int $page = 1): array
    {
        $query = Company::find()
            ->where(['!=', 'status', BaseModel::STATUS_DELETED])
            ->groupBy(['id']);

        $command = $query->createCommand();

        $dataProvider = new SqlDataProvider([
            'sql' => $command->rawSql,
            'pagination' => [
                'pageSize' => 55,
                'page' => $page - 1,
            ],
            'sort' => [
//                'attributes' => ['id', 'address', 'birth_date', 'last_name', 'first_name', 'father_name'],
            ],
        ]);
        $totalCount = $dataProvider->getTotalCount();
        $pagination = $dataProvider->getPagination();
        return [
            'dataProvider' => $dataProvider->getModels(),
            'pagination' => [
                'totalSize' => $totalCount,
                'page' => $pagination->page + 1,
                'sizePerPage' => $pagination->pageSize,
                'pageCount' => ceil($totalCount / $pagination->pageSize),
            ],
        ];
    }
}