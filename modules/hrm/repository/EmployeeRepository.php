<?php

namespace app\modules\hrm\repository;

use app\components\Constanta;
use app\modules\api\models\BaseModel;
use app\modules\hrm\models\HrEmployees;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class EmployeeRepository
{
    protected HrEmployees $hrEmployees;
    protected BaseModel $baseModel;
    public function __construct(HrEmployees $hrEmployees, BaseModel $baseModel)
    {
        $this->hrEmployees = $hrEmployees;
        $this->baseModel = $baseModel;
    }


	/**
	 * @return array
	 */
	public function getList($isSelect = false): array
	{
		$list = $this->hrEmployees::find()
			->alias('e')
			->select([
                    'e.id',
                    'e.address',
                    'e.birth_date',
                    'e.last_name',
                    'e.first_name',
                    'e.father_name',
                    'value' => "e.id",
                    'label' => new Expression("CONCAT(e.last_name,' ',e.first_name, ' ', e.father_name)"),
                ])
			->where(['e.status' => $this->baseModel::STATUS_ACTIVE]);
        return $list->asArray()->all();
	}

	/**
	 * @return HrEmployees[]|array|ActiveRecord[]
	 */
	public function getListMap(): array
	{
		return $this->hrEmployees::find()
			->select([
				'value' => 'id',
				'label' => "CONCAT(last_name, ' ', first_name, ' ', father_name)",
			])
			->where(['status' => $this->baseModel::STATUS_ACTIVE])
			->asArray()
			->all();
	}

	/**
	 * @param $data
	 * @param $id
	 * @return array
	 */
	public function saveModel($data, $id)
	{
		$transaction = Yii::$app->db->beginTransaction();
		try {
			if ($id != null) {
				$model = $this->hrEmployees::findOne($id);
			} else {
				$model = $this->hrEmployees;
				$model->type = Constanta::TYPE_EMPLOYEE_REGISTRATION;
			}
			if ($model->load($data, '') && $model->save()) {
                $transaction->commit();
                return [
                    'status' => true,
                    'model' => $model->getCreate(),
                ];
			}else{
                $transaction->rollBack();
                return [
                    'status' => false,
                    'errors' => $model->errors,
                ];
            }
		} catch (\Exception $e) {
			$transaction->rollBack();
			return [
				'status' => false,
				'errors' => $e->getMessage(),
			];
		}
	}

    /**
     * @param $id
     * @return array|ActiveRecord|null
     */
    public function findModel($id){
        return $this->hrEmployees::find()
            ->where(['id' => (int) $id])
            ->andWhere(['!=', 'status', BaseModel::STATUS_DELETED])
            ->one();
    }
}
