<?php

namespace app\modules\hrm\models\search;

use app\modules\hrm\models\BaseModel;
use app\modules\hrm\models\HrEmployees;
use yii\data\SqlDataProvider;

class HrEmployeesSearch extends HrEmployees
{
	public $hr_position_id;
	public $company_name;

	/**
	 * @return array[]
	 */
	public function rules()
	{
		return [
            [['id', 'status'], 'integer'],
            [['first_name', 'last_name', 'address', 'father_name','company_name'], 'safe']
        ];
	}

    /**
     * @param $params
     * @param int $page
     * @return array
     */
	public function search($params, int $page = 1): array
	{
        $company_id = \Yii::$app->user->identity->hrEmployee->company_id;
		$query = HrEmployees::find()
			->alias('e')
			->select([
				'id' => 'e.id',
				'address' => 'e.address',
				'last_name' => 'e.last_name',
				'first_name' => 'e.first_name',
				'father_name' => 'e.father_name',
				'company_name' => 'c.name',
				'company_id' => 'c.id',
			])
            ->leftJoin(['c' => 'company'],'e.company_id=c.id')
			->where(['!=', 'e.status', BaseModel::STATUS_DELETED])
            ->filterWhere(['e.company_id' => $company_id])
			->groupBy(['e.id']);

		if (empty($params['sort'])) {
			$query->orderBy(['e.updated_at' => SORT_DESC]);
		}

		$this->load($params, '');

		$query->andFilterWhere([
			'e.id' => $this->id,
			'e.status' => $this->status,
		]);

		$query
			->andFilterWhere(['=', 'e.father_name', $this->father_name])
			->andFilterWhere(['=', 'e.first_name', $this->first_name])
			->andFilterWhere(['=', 'e.last_name', $this->last_name])
			->andFilterWhere(['=', 'e.address', $this->address])
			->andFilterWhere(['=', 'c.name', $this->company_name]);

		$command = $query->createCommand();

		$dataProvider = new SqlDataProvider([
			'sql' => $command->rawSql,
			'pagination' => [
				'pageSize' => 55,
				'page' => $page - 1,
			],
			'sort' => [
				'attributes' => ['id', 'address', 'birth_date', 'last_name', 'first_name', 'father_name'],
			],
		]);
		$totalCount = $dataProvider->getTotalCount();
		$pagination = $dataProvider->getPagination();
		return [
			'dataProvider' => $dataProvider->getModels(),
			'pagination' => [
				'totalSize' => $totalCount,
				'page' => $pagination->page + 1,
				'sizePerPage' => $pagination->pageSize,
				'pageCount' => ceil($totalCount / $pagination->pageSize),
			],
		];
	}


    /**
     * @param int $page
     * @return array
     */
	public function page(int $page = 1): array
	{
		$query = HrEmployees::find()
			->alias('e')
            ->leftJoin(['c' => 'company'],'e.company_id=c.id')
			->select([
				'id' => 'e.id',
				'address' => 'e.address',
				'last_name' => 'e.last_name',
				'first_name' => 'e.first_name',
				'father_name' => 'e.father_name',
                'company_name' => 'c.name',
			])
			->where(['!=', 'e.status', BaseModel::STATUS_DELETED])
			->groupBy(['e.id']);

		$command = $query->createCommand();

		$dataProvider = new SqlDataProvider([
			'sql' => $command->rawSql,
			'pagination' => [
				'pageSize' => 55,
				'page' => $page - 1,
			],
			'sort' => [
				'attributes' => ['id', 'address', 'birth_date', 'last_name', 'first_name', 'father_name'],
			],
		]);
		$totalCount = $dataProvider->getTotalCount();
		$pagination = $dataProvider->getPagination();
		return [
			'dataProvider' => $dataProvider->getModels(),
			'pagination' => [
				'totalSize' => $totalCount,
				'page' => $pagination->page + 1,
				'sizePerPage' => $pagination->pageSize,
				'pageCount' => ceil($totalCount / $pagination->pageSize),
			],
		];
	}


}
