<?php

namespace app\modules\admin\repository;

use app\modules\admin\models\AuthAssignmentSearch;

class AuthAssignmentSearchRepository
{
    protected AuthAssignmentSearch $authAssignmentSearch;

    public function __construct(AuthAssignmentSearch $authAssignmentSearch)
    {
        $this->authAssignmentSearch = $authAssignmentSearch;
    }
    public function getUserRolesForSelect($id): array
    {
        $query = $this->authAssignmentSearch::find()
            ->alias('aa')
            ->select([
                'value' => 'ai.name',
                'label' => 'ai.name_for_user',
            ])
            ->where(['aa.user_id' => $id])
            ->leftJoin('auth_item ai', 'aa.item_name = ai.name')
            ->orderBy(['ai.created_at' => SORT_ASC]);

        return $query->asArray()->all();
    }
}