<?php

namespace app\modules\hrm\repository;

use app\modules\hrm\models\BaseModel;
use app\modules\hrm\models\Company;
use Yii;

class CompanyRepository
{
    protected Company $company;
    protected BaseModel $baseModel;
    public function __construct(Company $company, BaseModel $baseModel)
    {
        $this->company = $company;
        $this->baseModel = $baseModel;
    }


    /**
     * @return array
     */
    public function getList($isSelect = false): array
    {
        $list = $this->hrEmployees::find()
            ->alias('e')
            ->select([
                'e.id',
                'e.address',
                'e.birth_date',
                'e.last_name',
                'e.first_name',
                'e.father_name',
                'value' => "e.id",
                'label' => new Expression("CONCAT(e.last_name,' ',e.first_name, ' ', e.father_name)"),
            ])
            ->where(['e.status' => $this->baseModel::STATUS_ACTIVE]);
        return $list->asArray()->all();
    }

    /**
     * @return HrEmployees[]|array|ActiveRecord[]
     */
    public function getListMap(): array
    {
        return $this->hrEmployees::find()
            ->select([
                'value' => 'id',
                'label' => "CONCAT(last_name, ' ', first_name, ' ', father_name)",
            ])
            ->where(['status' => $this->baseModel::STATUS_ACTIVE])
            ->asArray()
            ->all();
    }

    /**
     * @param $data
     * @param $id
     * @return array
     */
    public function saveModel($data, $id)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $model = $this->company;
            if ($id != null) {
                $model = $this->company::findOne($id);
            }
            if ($model->load($data, '') && $model->save()) {
                $transaction->commit();
                return [
                    'status' => true,
                    'model' => $model->getCreate(),
                ];
            }else{
                $transaction->rollBack();
                return [
                    'status' => false,
                    'errors' => $model->errors,
                ];
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            return [
                'status' => false,
                'errors' => $e->getMessage(),
            ];
        }
    }

    /**
     * @param $id
     * @return array|ActiveRecord|null
     */
    public function findModel($id){
        return $this->company::find()
            ->where(['id' => (int) $id])
            ->andWhere(['!=', 'status', BaseModel::STATUS_DELETED])
            ->one();
    }
}