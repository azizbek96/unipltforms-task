<?php

namespace app\modules\api\controllers;

use app\components\PermissionHelper as P;
use app\modules\hrm\models\search\CompanySearch;
use app\modules\hrm\repository\CompanyRepository;
use Yii;
use yii\db\Exception;
use app\controllers\BaseController;
use app\modules\api\models\BaseModel;

class CompanyController extends BaseController
{
    protected  $repository;
    public function __construct($id, $module, $config = [], CompanyRepository $repository )
    {
        $this->repository = $repository;
        parent::__construct($id, $module, $config);
    }

    /**
	 * @param int $page
	 * @return array
	 * @throws Exception
	 */
	public function actionIndex(int $page = 1): array
	{
		if ($this->request->isGet) {
			return $this->getCompany($page);
		}
		return $this->error([], Yii::t('app', 'The query is incorrect'));
	}

    /**
     * @param $id
     * @return array
     */
	public function actionView($id)
	{
		if ($this->request->isGet) {
			$model = $this->findModel($id);
			if (!empty($model['code']) && $model['code'] == BaseModel::CODE_ERROR) {
				return $model;
			} else {
				return $this->success($model);
			}
		}
		return $this->error();
	}

	/**
	 * @return array
	 */
	public function actionCreate()
	{
		if ($this->request->isPost) {
			return $this->extracted(null);
		}
		return $this->error();
	}

    /**
     * @param $id
     * @return array
     */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if (empty($model)) {
			return $this->error([], Yii::t('app', 'Not Found'), 404);
		}

		if ($this->request->isPut) {
			return $this->extracted($id);
		}

		if ($this->request->isGet) {
			return $this->success($model->getUpdate($id));
		}
		return $this->error();
	}

    /**
     * @param $id
     * @return array
     */
	public function actionDelete($id)
	{
		if ($this->request->isDelete) {
			$model = $this->findModel($id);
			if (!empty($model['code']) && $model['code'] == BaseModel::CODE_ERROR) {
				return $model;
			} else {
				$response = $model->isDelete();
				return $response['status'] ? $this->getCompanyPage($page = 1, "Company successfully deleted") : $this->error($response['errors']);
			}
		} else {
			return $this->error();
		}
	}
	/**
	 * @param $id
	 * @return array
	 */
	private function extracted($id = null): array
	{
		$data = $this->request->post();
		$response = $this->repository->saveModel($data, $id);
		return $response['status'] ? $this->success($response['model'], [], Yii::t('app', 'Company successfuly created')) : $this->error($response);
	}

    /**
     * @param $id
     * @return array
     */
	protected function findModel($id)
	{
		$model = $this->repository->findModel($id);
		return $model !== null ? $model : $this->error();
	}

    /**
     * @param int $page
     * @return array
     */
	private function getCompany(int $page = 1): array
	{
		$searchModel = new CompanySearch();
		$dataProvider = $searchModel->search($this->request->queryParams, $page);
		return $this->success($dataProvider['dataProvider'], [
			'pagination' => $dataProvider['pagination'],
            'permissions' => [
                'create' => P::can('company/create'),
                'view' => P::can('company/view'),
                'delete' => P::can('company/delete'),
                'update' => P::can('company/update'),
            ],
		]);
	}


	/**
	 * @param int $page
	 * @param null $departmentId
	 * @return array
	 */
	private function getCompanyPage(int $page = 1, $message = ''): array
	{
		$searchModel = new CompanySearch();
		$dataProvider = $searchModel->page($page);

		return $this->success($dataProvider['dataProvider'], [
			'pagination' => $dataProvider['pagination'],
		], $message);
	}
}
