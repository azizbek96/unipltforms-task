-- MySQL dump 10.13  Distrib 8.0.30, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: task_unipltforms
-- ------------------------------------------------------
-- Server version	8.0.30-0ubuntu0.20.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_assignment`
--

DROP TABLE IF EXISTS `auth_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8mb3_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8mb3_unicode_ci NOT NULL,
  `created_at` int DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  KEY `idx-auth_assignment-user_id` (`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_assignment`
--

LOCK TABLES `auth_assignment` WRITE;
/*!40000 ALTER TABLE `auth_assignment` DISABLE KEYS */;
INSERT INTO `auth_assignment` VALUES ('admin','1',NULL),('company','3',NULL),('user','2',NULL);
/*!40000 ALTER TABLE `auth_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8mb3_unicode_ci NOT NULL,
  `type` smallint NOT NULL,
  `description` text COLLATE utf8mb3_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int DEFAULT NULL,
  `updated_at` int DEFAULT NULL,
  `role_type` int DEFAULT NULL,
  `name_for_user` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `category` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item`
--

LOCK TABLES `auth_item` WRITE;
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;
INSERT INTO `auth_item` VALUES ('admin',1,'',NULL,NULL,NULL,NULL,NULL,'Admin',NULL),('auth-item/create-permission',2,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('auth-item/create-rule',2,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('auth-item/delete',2,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('auth-item/index-permission',2,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('auth-item/index-rule',2,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('auth-item/search',2,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('auth-item/update-permission',2,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('auth-item/update-rule',2,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('auth-item/view',2,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('company',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('company-role',1,'',NULL,NULL,NULL,NULL,NULL,'Fayl yulash role',NULL),('company/create',2,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('company/delete',2,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('company/index',2,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('company/update',2,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('company/view',2,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('employee',1,'',NULL,NULL,NULL,NULL,NULL,'Foydalanuvchi role',NULL),('employee/create',2,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('employee/delete',2,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('employee/index',2,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('employee/update',2,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('employee/view',2,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('moderator',1,'',NULL,NULL,NULL,NULL,NULL,'Moderator',NULL),('role-auth-item',1,'',NULL,NULL,NULL,NULL,NULL,'Ruxsat va role permission',NULL),('role-user',1,'',NULL,NULL,NULL,NULL,NULL,'User uchun ruxsatlar',NULL),('user',1,'',NULL,NULL,NULL,NULL,NULL,'User',NULL),('users/create',2,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('users/delete',2,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('users/index',2,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('users/update',2,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('users/view',2,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8mb3_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item_child`
--

LOCK TABLES `auth_item_child` WRITE;
/*!40000 ALTER TABLE `auth_item_child` DISABLE KEYS */;
INSERT INTO `auth_item_child` VALUES ('role-auth-item','auth-item/create-permission'),('role-auth-item','auth-item/create-rule'),('role-auth-item','auth-item/delete'),('role-auth-item','auth-item/index-permission'),('role-auth-item','auth-item/index-rule'),('role-auth-item','auth-item/search'),('role-auth-item','auth-item/update-permission'),('role-auth-item','auth-item/update-rule'),('role-auth-item','auth-item/view'),('admin','company-role'),('company-role','company/create'),('company-role','company/delete'),('company','company/index'),('company-role','company/index'),('company','company/update'),('company-role','company/update'),('company','company/view'),('company-role','company/view'),('admin','employee'),('company','employee'),('employee','employee/create'),('employee','employee/delete'),('employee','employee/index'),('employee','employee/update'),('employee','employee/view'),('admin','role-auth-item'),('admin','role-user'),('role-user','users/create'),('role-user','users/delete'),('role-user','users/index'),('role-user','users/update'),('role-user','users/view');
/*!40000 ALTER TABLE `auth_item_child` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8mb3_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int DEFAULT NULL,
  `updated_at` int DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_rule`
--

LOCK TABLES `auth_rule` WRITE;
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `company` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `leader_name` varchar(255) DEFAULT NULL,
  `address` text,
  `email` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `type` smallint DEFAULT NULL,
  `status` smallint DEFAULT '1',
  `created_at` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `updated_at` int DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` VALUES (2,'Orzu Grand','Azizbek Ismoilov','Farg\'ona','orzu@grand.com','orzu.com','+998911567144',NULL,1,1664504017,1,1664504017,NULL),(3,'DataRubic LLC','Avazbek Ismoilov','Farg\'ona','data@gmail.com','data.com','+998997867144',NULL,1,1664504052,1,1664505319,1),(4,'OmadDev LLC','Avazbek Ismoilov','Farg\'ona','data@grand.com','data.com','+998911567144',NULL,0,1664504056,1,1664504593,1),(5,'AvazbeProdaction LLC','Avazbek Ismoilov','Farg\'ona','data@grand.com','data.com','+998911567144',NULL,1,1664504078,1,1664504078,NULL),(6,'Dkshjad LLC','asd Ismoilov','Farg\'ona','sf@grand.com','daata.com','+998911567144',NULL,1,1664506382,1,1664506382,NULL),(7,'Dkshjad LLC','asd Ismoilov','Farg\'ona','sf@grand.com','daata.com','+998911567144',NULL,1,1664506635,1,1664506635,NULL);
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr_employees`
--

DROP TABLE IF EXISTS `hr_employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hr_employees` (
  `id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `father_name` varchar(255) DEFAULT NULL,
  `address` text,
  `phone_number` varchar(50) DEFAULT NULL,
  `passport` varchar(50) DEFAULT NULL,
  `position` varchar(50) DEFAULT NULL,
  `type` smallint DEFAULT NULL,
  `status` smallint DEFAULT '1',
  `created_at` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `updated_at` int DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `company_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx-hr_employees-company_id` (`company_id`),
  CONSTRAINT `fk-hr_employees-company_id` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr_employees`
--

LOCK TABLES `hr_employees` WRITE;
/*!40000 ALTER TABLE `hr_employees` DISABLE KEYS */;
INSERT INTO `hr_employees` VALUES (1,'Azizbek','Ismoilov','Abdullajon','Bogdod','+998-33-786-71-44','AA1231445','hodim',1,1,1664503829,1,1664503829,1,NULL),(2,'Ahadjon','Ismoilov','Abdullajon','Bogdod','+998-91-156-71-44','AA1231445','hodim',1,1,1664503829,1,1664503829,1,NULL),(3,'Avazbek','Ismoilov','Abdullajon','Bogdod','+998-90-508-22-56','AA1231445','hodim',1,1,1664503829,1,1664503829,1,2),(6,'Odina','Ismoilov','Abdurashidjon qizi','Bogdod','997864471','AA1231445','hodim',1,1,1664520427,1,1664520640,1,3);
/*!40000 ALTER TABLE `hr_employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1664503826),('m140506_102106_rbac_init',1664503827),('m170907_052038_rbac_add_index_on_auth_assignment_user_id',1664503827),('m180523_151638_rbac_updates_indexes_without_prefix',1664503827),('m200409_110543_rbac_update_mssql_trigger',1664503827),('m211210_072053_create_hr_employees_table',1664503829),('m211210_072054_create_users_table',1664503832),('m220604_072038_add_role_type_column_to_name_for_user_column_to_auth_item_table',1664503832),('m220604_072738_add_role_to_first_user',1664503832),('m220604_072740_create_user_refresh_tokens_table',1664503832),('m220719_045051_insert_rbac_permissions',1664503832),('m220719_045055_insert_rbac_permissions',1664503832),('m220719_045059_insert_rbac_permissions',1664503832),('m220719_045060_insert_rbac_permissions',1664503832),('m220930_011146_create_company_table',1664503832),('m220930_011259_add_company_id_column_to_hr_employees_table',1664503832);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_refresh_tokens`
--

DROP TABLE IF EXISTS `user_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_refresh_tokens` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `token` varchar(1000) DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `user_agent` varchar(1000) DEFAULT NULL,
  `status` smallint DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `updated_at` int DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx-user_refresh_tokens-urf_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_refresh_tokens`
--

LOCK TABLES `user_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `user_refresh_tokens` DISABLE KEYS */;
INSERT INTO `user_refresh_tokens` VALUES (1,3,'ADdyersEfD4P8iB39KnpWxIVBUnLdtgyITBmL4V6E0pVm58lqIF-EJi5M5OlkQNX_7egpJ1DKoNybwmFZ9w0jXxD5npBLu4k3tC1r7tHEI8JC0PpMNlhW20223XUGIVhsdj1I3gT9od5_tE4QLEm-MfxcU85yCF-ZBAWgdxh88mIV7wj1yL663jEVnIA0wlrD2UUkE65','127.0.0.1','PostmanRuntime/7.26.8',NULL,1664504718,3,1664504718,NULL),(2,1,'z4a2uPtH7sQANF-G51PuEkShrpWsQ-Ii9vG8LpGIRemawA-MPx3h3mAC7z2v6XAoHcd-8e1IuK9AkAN9cBFI9YwJZiCdsybqUT5Xkh66uCj3DwsMdqj7wfT4uph44WrtSSjl9x1PTDSjCt6qRZ6cQ-Qm3rVXTILTZZNCev2QYLTmFpk_7qASEIIwg3_AUZGRe4N9JwXf','127.0.0.1','PostmanRuntime/7.26.8',NULL,1664505128,1,1664505128,NULL),(3,3,'-57uzdCuLtm9OQl4V_cjZOF43_hTxHZD_qrGHsw7snfpMzc8kRxkqU5KmXV324xKasDAMV4_R014N5AIvPCMOzMun7Xxn-JsMpV5gBezTRwvVZkOv4V-LC5oBZMftnuMs0mThnrWKGUWzfJLddFTWiWDDUDZH5VG2XENCvqVJnZFWbQzIj-M1v2UYZAKZm316Y0iGKzk','127.0.0.1','PostmanRuntime/7.26.8',NULL,1664506315,3,1664506315,NULL),(4,1,'vL1DlnhpnuOK3qi0ohjKHasjX8PnR-GvZr_nUe0EXAlUuSQbIY8MyBX_iDYa1t6w0pJoBV3kdMKnlgEHhOZIF09-z4AQ3-DWDRMJv65GFpuK2YUG3Ef8PGFwWHiT1lerqE53ngDJ26Bh4CLICPHA3Y-CIHImDNLlZ7N-xDfQps1LWAwtFMB4lZvh7B0RTOvCYjv1pUUO','127.0.0.1','PostmanRuntime/7.26.8',NULL,1664508615,1,1664508615,NULL),(5,1,'zEigeteTbgHPVqAOjQuP5sBw_2Q6T1f7Ty1fDg2WsrwcQIIxaMrupxZY6QLoSqq_Cu1OaAIEJlZ7aEPO84D1yC0_iJLs6cyyYkyORDH53BEGuRYaLKn-ODIIMTqvaCyBkEkLGD3MSBMtzLN2FOBxIeZ9aOAOEDUUXEbMHkChorWgddJrDUpWGa5cjTNIqqpWaH6GJB-y','127.0.0.1','PostmanRuntime/7.26.8',NULL,1664519923,1,1664519923,NULL),(6,1,'7-EsQHFfeW1dsBQWJn8N86DPavagOtR7fiiuEr6E1YslGHaJgH9836QqF9Sv-zGIHcdRTkqi3PCrycynBfNlwXvg0AyBegW5HtVStF7ezHO5FKVZ-f7ODxitwASesgS28hPJV6FHxE_Tt-jnBfAuYBxSoC-FCCL5zC3mc_wv4oTFF6alWT15OjGqNz_70mfBwFaoAJXQ','127.0.0.1','PostmanRuntime/7.26.8',NULL,1664520096,1,1664520096,NULL);
/*!40000 ALTER TABLE `user_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password_hash` varchar(255) DEFAULT NULL,
  `hr_employee_id` int DEFAULT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `auth_key` varchar(255) DEFAULT NULL,
  `status` smallint DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `updated_at` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx-users-hr_employee_id` (`hr_employee_id`),
  CONSTRAINT `fk-users-hr_employee_id` FOREIGN KEY (`hr_employee_id`) REFERENCES `hr_employees` (`id`) ON DELETE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','$2y$13$mvEy1ewd3aiUvkYhH2SIH.D8jsU6NHInsIrSBlHpDZYroVLPAlyfO',1,NULL,'qSn5DH7kIedrNo1YXIpZFNz-f67qWF8U',1,1,1664503830,1,1664503830),(2,'user','$2y$13$182FJh1LYfHQr3E3a.G5gevI6IlLYM65HzBYYasiqpZwDHoX55XyK',2,NULL,'qSn5DH7kIedrNo1YXIpZFNz-f67qWF8U',1,1,1664503831,1,1664503831),(3,'company','$2y$13$.sWDiF81adczf0MslyfcLe0zCmBmfCGf9oCAhgsqxNJadFs8FIPD.',3,NULL,'qSn5DH7kIedrNo1YXIpZFNz-f67qWF8U',1,1,1664503832,1,1664503832);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-09-30 11:52:51
