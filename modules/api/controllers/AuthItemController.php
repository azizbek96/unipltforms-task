<?php

namespace app\modules\api\controllers;

use app\components\PermissionHelper as P;
use Yii;
use yii\base\Exception;
use yii\db\StaleObjectException;
use app\controllers\BaseController;
use app\modules\api\models\BaseModel;
use app\modules\admin\models\AuthItem;
use app\modules\admin\models\AuthItemChild;
use app\modules\admin\models\AuthItemSearch;
use app\modules\api\responses\AuthItemResponse;
use app\modules\admin\repository\MenuRepository;
use app\modules\admin\repository\AuthItemRepository;
use app\modules\admin\repository\AuthItemSearchRepository;

class AuthItemController extends BaseController
{
    public string $type = 'role';
    protected MenuRepository $menuRepository;
    protected AuthItemRepository $authItemRepository;
    protected AuthItemSearchRepository $authItemSearchRepository;

    public function __construct($id, $module, $config = [],
        MenuRepository $menuRepository,
        AuthItemRepository $authItemRepository,
        AuthItemSearchRepository $authItemSearchRepository
    )
    {
        parent::__construct($id, $module, $config);
        $this->menuRepository = $menuRepository;
        $this->authItemRepository = $authItemRepository;
        $this->authItemSearchRepository = $authItemSearchRepository;
    }

    /**
     * @param $page
     * @return array
     */
    public function actionIndexRole($page = 1)
    {
        if (Yii::$app->request->isGet) {
            $params = Yii::$app->request->queryParams;
            $authItemSearch = $this->authItemSearchRepository->indexRole($params, $page);
            return $this->success($authItemSearch['dataProvider'],$authItemSearch['additionalData'] );
        } else {
            return $this->error();
        }
    }

    public function actionSearch($role = false)
    {
        $params = $this->request->queryParams;
        return $this->authItemRepository->search($params, $role);
    }

    /**
     * @return array
     */
    public function actionIndexPermission()
    {
        $params = Yii::$app->request->queryParams;
        $authItemRepository = $this->authItemRepository->getSearch($params);
        return $this->success($authItemRepository['dataProvider'], $authItemRepository['additional']);
    }

    /**
     * @param $id
     * @return array
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isGet) {
            $model = $this->findModel($id);
            return $this->success($model);
        } else {
            return $this->error();
        }
    }

    public function actionCreateRole()
    {
        $request = Yii::$app->request;
        if ($request->isGet) {
            return $this->success(['role_type' => $this->authItemRepository->getRoleType()]);
        }
        if ($request->isPost) {
            $data = $request->post();
            $model = $this->authItemRepository->createRole($data);
            if ($model->save()) {
                return $this->success(AuthItemResponse::create($model));
            } else {
                return $this->error(['errors' => $model->getErrors()]);
            }
        }
        return $this->error();
    }

    /**
     * @param $name
     * @return array
     */
    public function actionUpdateRole($name = null): array
    {
        $request = Yii::$app->request;
        $model = $this->findModel($name);
        if ($request->isGet) {
            return $this->success([
                'roleName' => $name,
                'role_type' => $this->authItemRepository->getRoleType(),
                'role' => AuthItemResponse::update($model),
            ]);
        }
        if ($request->isPut) {
            if (!empty($model['code']) && $model['code'] == BaseModel::CODE_ERROR) {
                return $model;
            } else {
                return $this->authItemRepository->updateRole($model);
            }
        }
        return $this->error();
    }

    /**
     * @return array
     * @throws Exception
     */
    public function actionCreatePermission()
    {
        $model = new AuthItem();
        if (Yii::$app->request->isGet) {
            return $this->success([
                'categories' => $model->getCategory(true),
            ]);
        }
        if (Yii::$app->request->isPost) {
            return $this->authItemRepository->createPermission($model);
        }
        return $this->error();
    }

    /**
     * @param $id
     * @return array
     * @throws Exception
     */
    public function actionUpdatePermission($name)
    {
        $model = $this->findModel($name);
        $model->type = 2;
        if (Yii::$app->request->isGet) {
            return $this->success([
                'values' => AuthItemResponse::update($model),
                'categories' => $model->getCategory(true),
            ]);
        } elseif (Yii::$app->request->isPut) {
            $data = Yii::$app->request->post();
            $data['type'] = 2;
            if ($model->load($data, '') && $model->save()) {
                $auth = Yii::$app->authManager;
                $this->deletePermission($model->name);
                $role = $auth->getRole($model->category);
                $permission = $auth->getPermission($model->name);
                $auth->addChild($role, $permission);

                $authItemRepository = $this->authItemRepository->getSearch();
                return $this->success($authItemRepository['dataProvider'], $authItemRepository['additional']);
            } else {
                return $this->error(['errors' => $model->errors]);
            }
        }
        return $this->error();
    }

    /**
     * @param $id
     * @return array
     * @throws StaleObjectException
     */
    public function actionDelete($name)
    {
        if (Yii::$app->request->isDelete) {
            $model = $this->findModel($name);
            if (!empty($model)) {
                if ($model->delete()) {
                    return $this->success();
                } else {
                    return $this->error(['errors' => $model->errors]);
                }
            } else {
                return $this->error([], Yii::t('app', 'Not found'));
            }
        }
        return $this->error();
    }

    /**
     * @param $perm
     * @return void
     * @throws StaleObjectException
     */
    protected function deletePermission($perm)
    {
        $model = AuthItemChild::find()
            ->where(['child' => $perm])
            ->one();
        if (!empty($model)) {
            $model->delete();
        }
    }

    public function actionMenu()
    {
        if (Yii::$app->request->isGet) {
            return $this->success($this->menuRepository->menu());
        }
        return $this->error([]);
    }

    /**
     * @param string $name
     * @return AuthItem|array|\yii\db\ActiveRecord
     */
    private function findModel(string $name)
    {
        $model = $this->authItemRepository->findModel($name);
        return $model !== null ? $model : $this->error([], Yii::t('app', 'Not found'));
    }
}
