<?php

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;

class MycommandController extends Controller
{
    public function actionWelcome($message = 'Hello world!')
    {
        echo $message . "\n";

        return ExitCode::OK;
    }
}