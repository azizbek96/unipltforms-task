<?php

use yii\db\Migration;

/**
 * Class m220604_072738_add_role_to_first_user
 */
class m220604_072738_add_role_to_first_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->upsert('auth_item', ['name' =>'admin', 'type' => '1', 'description' => '', 'name_for_user' => 'Admin']);
        $this->upsert('auth_item', ['name' => 'user', 'type' => '1', 'description' => '', 'name_for_user' => 'User']);
        $this->upsert('auth_item', ['name' => 'moderator', 'type' => '1', 'description' => '', 'name_for_user' => 'Moderator']);
        $this->upsert('{{%auth_assignment}}', [
            'item_name' => 'admin',
            'user_id' => 1,
        ], [
            'item_name' => 'admin',
            'user_id' => 1,
        ]);
        $this->upsert('{{%auth_assignment}}', [
            'item_name' => 'user',
            'user_id' => 2,
        ], [
            'item_name' => 'user',
            'user_id' => 2,
        ]);

        $this->upsert('{{%auth_assignment}}', [
            'item_name' => 'moderator',
            'user_id' => 3,
        ], [
            'item_name' => 'moderator',
            'user_id' => 3,
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%auth_assignment}}', ['user_id' => 1]);
        $this->delete('{{%auth_assignment}}', ['user_id' => 2]);
        $this->delete('{{%auth_assignment}}', ['user_id' => 3]);
        $this->delete('auth_item', ['name' =>'admin', 'type' => '1']);
        $this->delete('auth_item', ['name' => 'user', 'type' => '1']);
        $this->delete('auth_item', ['name' => 'moderator', 'type' => '1']);

    }

}
