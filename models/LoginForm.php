<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\modules\admin\models\Users;

/**
 * LoginForm is the model behind the login form.
 *
 * @property-read Users|null $user
 *
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user = false;
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [["username", 'password'], 'required'],
            ['rememberMe', 'boolean'],
            ['password', 'validatePassword'],
            [['password'], 'trim'],
        ];
    }

	/**
	 * Validates the password.
	 * This method serves as the inline validation for password.
	 *
	 * @param string $attribute the attribute currently being validated
	 * @param array $params the additional name-value pairs given in the rule
	 */
	public function validatePassword($attribute, $params)
	{
		if (!$this->hasErrors()) {
			$user = $this->getUser();

			if (!$user || !$user->validatePassword($this->password)) {
				$this->addError($attribute, Yii::t('app', 'Incorrect username or password.'));
				$this->addError('username', Yii::t('app', 'Incorrect username or password.'));
			}
		}
	}

    /**
     * @return bool
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
        }
        return false;
    }

	/**
	 * Finds user by [[username]]
	 *
	 * @return Users|null
	 */
	public function getUser()
	{
		if ($this->_user === false) {
			$this->_user = Users::findByUsername($this->username);
		}

		return $this->_user;
	}
}
